2048

Authors:

Riccardo Marconi - Michele Prencipe Pio - Sara Valdiserri

Instructions:

Download and run 2048.jar.
Use the mouse to select the menu you want and use "A" "S" "D" "W" to move the grid and play.

REPORT: https://bitbucket.org/michtor99/oop19-marconi-riccardo-prencipe-michele-valdiserri-sara-2048/raw/7012ad3e72d2d0d7aa38866571cdaebbac3788c4/reports2048.pdf


JAR: https://bitbucket.org/michtor99/oop19-marconi-riccardo-prencipe-michele-valdiserri-sara-2048/raw/7012ad3e72d2d0d7aa38866571cdaebbac3788c4/2048.jar
